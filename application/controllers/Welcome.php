<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent ::__construct();
		$this->load->model('demoModel','dm',TRUE);
	}
	
	public function index() {
		$test = base_url().'demo-func';
		echo $test; exit;
		redirect($test);
		
	}

	public function demoFunction() {
		$result['name'] = $this->dm->demoFunc();
		$this->load->view('welcome_message',$result);
	}
}
